#!/bin/sh

############################################################
# IN : message enregistrer
# OUT :
############################################################
function log {
DATE=`date +%Y-%m-%d_%H-%M`
cat << EOF

// ${DATE}
$@
---------------------
EOF
}

############################################################
# IN : chemin du fichier de log
# OUT :
############################################################
function log_start() {
  # Sauvegarde des descripteurs 0, 1 et 2 dans 3, 4 et 5
  exec 3<&0
  exec 4<&1
  exec 5<&2
  # Creation du descripteur 6 redirige dans le fichier passe en param
  exec 6>"$1"
  # Redirection des descripteurs 1 et 2 vers le 6
  exec 1>&6
  exec 2>&6
}

############################################################
# IN :
# OUT :
############################################################
function log_stop() {
  # Restauration des descripteurs 1 et 2
  exec 1<&4
  exec 2<&5
  # Fermeture des descripteurs 3, 4, 5 et 6
  exec 3>&-
  exec 4>&-
  exec 5>&-
  exec 6>&-
}

############################################################
# IN : fichier, sujet, dest1, destn...
# OUT :
############################################################
function mail_file() {
  FILE="$1"
  SUBJECT="$2"
  shift 2
  mail -s "${SUBJECT}" "$@" < "${FILE}"
}

############################################################
# IN : PID du programme
# OUT :
############################################################
function single_run_pid() {
  CMD=`cat /proc/$1/cmdline | xargs -0 echo `
  NB=`ps x | grep "$CMD" | wc -l`
  if [ "$NB" -gt 3 ]; then
    exit
  fi
}
