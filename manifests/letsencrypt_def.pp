# Manager Letsencrypt key
define sys::letsencrypt_def (
  String $acme_path,
  String $http_server = 'nginx',
  Boolean $pre_hook_http_server = true,
  Enum['standalone', 'webroot'] $renew = 'webroot',
  $cert_domains = undef,
) {

  require letsencrypt

  if $renew == 'webroot' {
    ensure_resource('sys::letsencrypt_acme', $acme_path, {})
    $manage_cron = false
  } else {
    $manage_cron = true
  }

  if ! $cert_domains {
    $domains = [$name]
  } else {
    $domains = $cert_domains
  }

  if $pre_hook_http_server {
    letsencrypt::certonly { $name:
      domains            => $domains,
      additional_args    => ['--keep', '--non-interactive'],
      plugin             => 'standalone',
      pre_hook_commands  => "/usr/bin/systemctl stop ${http_server}",
      post_hook_commands => "/usr/bin/systemctl is-active ${http_server} && /usr/bin/systemctl reload ${http_server}",
      manage_cron        => $manage_cron,
    }
  } else {
    letsencrypt::certonly { $name:
      domains            => $domains,
      additional_args    => ['--keep', '--non-interactive'],
      plugin             => 'standalone',
      post_hook_commands => "/usr/bin/systemctl is-active ${http_server} && /usr/bin/systemctl reload ${http_server}",
      manage_cron        => $manage_cron,
    }
  }

  if $renew == 'webroot' {
    $execution_environment = [ "VENV_PATH=${letsencrypt::venv_path}" ]
    $_domains = join($domains, '\' -d \'')
    $cron_cmd = "certbot --text --agree-tos --non-interactive certonly --rsa-key-size 4096 -a webroot --cert-name '${name}' --webroot-path ${acme_path} -d '${_domains}' --keep --non-interactive --keep-until-expiring"
    file { "${letsencrypt::cron_scripts_path}/renew-${title}.sh":
      ensure  => file,
      mode    => '0755',
      owner   => 'root',
      group   => $letsencrypt::cron_owner_group,
      content => template('letsencrypt/renew-script.sh.erb'),
    }

    cron { "letsencrypt renew cron ${title}":
      ensure   => present,
      command  => "\"${letsencrypt::cron_scripts_path}/renew-${name}.sh\"",
      user     => 'root',
      hour     => fqdn_rand(24, $name),
      minute   => fqdn_rand(60, fqdn_rand_string(10, $name)),
      monthday => '*',
    }
  }

}
