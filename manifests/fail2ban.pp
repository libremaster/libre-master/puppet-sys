# Class: SYS / fail2ban
class sys::fail2ban (
  Boolean $sshd = false,
  Boolean $postfix = false,
  Boolean $dovecot = false,
  Boolean $pureftpd = false,
  Boolean $apache = false,
  Boolean $nginx = false,
  String $bantime = '24h',
  String $findtime = '1h',
  String $maxretry = '3',
  Array $ignoreips = []
) {

  if $sshd or $postfix or $dovecot or $pureftpd or $apache or $nginx {

    package{'fail2ban': ensure => present}

    file {
      '/etc/fail2ban/jail.local':
        content => template("${module_name}/fail2ban/etc/fail2ban/jail.local"),
        mode    => '0644',
        owner   => 'root',
        group   => 'root',
        require => Package['fail2ban'],
        notify  => Exec['fail2ban_reload'];
    }

    exec {
      'fail2ban_reload':
        command     => '/usr/bin/fail2ban-client reload',
        refreshonly => true,
    }

    service {
      'fail2ban':
        ensure    => running,
        hasstatus => true,
        enable    => true,
        require   => Package['fail2ban'];
    }

  } else {

    package{'fail2ban': ensure => absent}

  }

}
