# Class: SYS / APT
class sys::apt (
  String $email_notification,
  String $automatic_reboot,
  String $automatic_reboot_time,
  String $blacklist,
  Array[Hash] $sources=[],
) {

    require apt

    # APT CONF (APT SOURCE EN DEPEND)
    apt::conf {
      'install':
        content  =>  'APT::Install-Recommends "0";',
        priority =>  '99';
      'unattended-upgrades':
        content  =>  template('sys/apt/etc/apt/apt.conf.d/unattended-upgrades'),
        priority =>  '50';
      'periodic':
        content  =>  template('sys/apt/etc/apt/apt.conf.d/periodic'),
        priority =>  '10';
    }

    # APT SOURCE
    case $::operatingsystem {

      'Debian' : {
        apt::source {
          'apt_debian':
            location => 'http://ftp.debian.org/debian',
            repos    => 'main contrib non-free',
            release  => $::lsbdistcodename;
          'apt_debian_security':
            location => 'http://security.debian.org',
            repos    => 'main contrib non-free',
            release  => "${::lsbdistcodename}/updates";
          'apt_debian_updates':
            location => 'http://deb.debian.org/debian',
            repos    => 'main contrib non-free',
            release  => "${::lsbdistcodename}-updates";
        }
      }

      'Ubuntu' : {
        apt::source {
          'apt_ubuntu':
            location => 'http://fr.archive.ubuntu.com/ubuntu',
            repos    => 'main restricted universe',
            release  => $facts['os']['distro']['codename'];
          'apt_ubuntu_updates':
            location => 'http://fr.archive.ubuntu.com/ubuntu',
            repos    => 'main restricted universe ',
            release  => "${facts['os']['distro']['codename']}-updates";
          'apt_ubuntu_security':
            location => 'http://fr.archive.ubuntu.com/ubuntu',
            repos    => 'main restricted universe ',
            release  => "${facts['os']['distro']['codename']}-security";
        }
      }

      default: {
        fail(translate('Unsupported platform: puppetlabs-%{module_name} currently doesn\'t support %{os}.',
            {'module_name' => $module_name, 'os' => $::operatingsystem}))
      }

    }

    $sources.each |Integer $index, Hash[String, Variant[String, Hash]] $source| {
      apt::source { $source['name']:
        *   => $source['conf'],
      }
    }

}
