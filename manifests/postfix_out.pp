# Manage Postfix as a SMTP sender only
class sys::postfix_out (
  String $root_email = '',
  String $saslusername = '',
  String $saslpassword = '',
  Array[Hash] $dkim_keys = [],
  Array[Hash] $smtp_generic_maps = [],
) {
  # Désinstalle exim
  # ----------------
  package{'exim4-base': ensure => purged}
  package{'exim4-config': ensure => purged}
  package{'exim4-daemon-light': ensure => purged}

  # Install Postfix
  # ---------------
  include postfix

  postfix::config {
    'myhostname': value => $::fqdn;
    'smtpd_banner': value => '$myhostname';
    'disable_vrfy_command':
      ensure => present,
      value  => 'yes';
    'alias_database': value => 'hash:/etc/aliases';
    'virtual_alias_domains': ensure => absent;
    'virtual_mailbox_domains': ensure => absent;
    'virtual_mailbox_maps': ensure => absent;
    'virtual_mailbox_base': ensure => absent;
    'virtual_uid_maps': ensure => absent;
    'virtual_gid_maps': ensure => absent;
    'sender_bcc_maps': ensure => absent;
    'content_filter': ensure => absent;
    'smtpd_client_restrictions': ensure => absent;
    'smtpd_sender_restrictions': ensure => absent;
    'smtpd_helo_restrictions': ensure => absent;
    'proxy_read_maps': ensure => absent;
    'smtpd_sender_login_maps': ensure => absent;
    'relay_recipient_maps': ensure => absent;
    'relay_domains': ensure => absent;
    'smtpd_recipient_restrictions': ensure => absent;
    'greylisting': ensure => absent;
    'smtpd_restriction_classes': ensure => absent;
    'message_size_limit': ensure => absent;
    'receive_override_options': ensure => absent;
    'smtpd_sasl_path': ensure => absent;
    'smtpd_sasl_type': ensure => absent;
    'dovecot_destination_recipient_limit': ensure => absent;
    'smtp_tls_exclude_ciphers': ensure => absent;
    'smtpd_tls_exclude_ciphers': ensure => absent;
    'smtp_tls_protocols': ensure => absent;
    'smtpd_tls_protocols': ensure => absent;
    'smtpd_tls_mandatory_protocols': ensure => absent;
    'owner_request_special': ensure => absent;
    'body_checks': ensure => absent;
    'nested_header_checks': ensure => absent;
    'mime_header_checks': ensure => absent;
    'virtual_transport': ensure => absent;
    'maildrop_destination_recipient_limit': ensure => absent;
    'maildrop_destination_concurrency_limit': ensure => absent;
    'smtpd_client_message_rate_limit': ensure => absent;
    'smtpd_helo_required': ensure => absent;
    'smtpd_tls_security_level': ensure => absent;
    'smtpd_sasl_authenticated_header': ensure => absent;
    'broken_sasl_auth_clients': ensure => absent;
    'smtpd_sasl_auth_enable': ensure => absent;
  }

  if $saslusername != '' {
    postfix::hash { '/etc/postfix/sasl_passwd':
      ensure  => 'present',
      content => "${postfix::relayhost}  ${saslusername}:${saslpassword}",
    }
    postfix::hash { '/etc/postfix/canonical':
      ensure  => 'present',
      content => "root  ${root_email}",
    }
    postfix::config {
      'sender_canonical_maps': value => 'hash:/etc/postfix/canonical';
      'smtp_sasl_auth_enable': value => 'yes';
      'smtp_tls_security_level': value => 'encrypt';
      'smtp_sasl_tls_security_options': value => 'noanonymous';
      'smtp_sasl_password_maps': value => 'hash:/etc/postfix/sasl_passwd';
    }
  } else {
    postfix::config {
      'smtp_tls_security_level': ensure => absent;
    }
  }

  # Sensitive Header
  file {'/etc/postfix/header_checks':
    ensure  => present,
    owner   => 'postfix',
    group   => 'root',
    mode    => '0600',
    content => "/^X-Mailer:/            IGNORE
"
  }
  postfix::config {
    'header_checks': value => 'regexp:/etc/postfix/header_checks', require => File['/etc/postfix/header_checks'];
  }

  # DKIM / Rewriting
  if $dkim_keys != [] {

    $smtp_generic_maps_content = $smtp_generic_maps.map |Integer $index, Hash[Enum['syssender', 'pubsender'], String] $smtp_generic_map| {
      "${smtp_generic_map['syssender']} ${smtp_generic_map['pubsender']}"
    }.join("\n")

    postfix::hash {'/etc/postfix/generic':
        ensure  => 'present',
        content => $smtp_generic_maps_content;
    }

    postfix::config {
      'smtp_generic_maps': value => 'hash:/etc/postfix/generic';
    }

    # opendkim
    ensure_packages(['opendkim', 'opendkim-tools'])

    service {
      'opendkim':
        ensure    => running,
        hasstatus => true,
        enable    => true,
        require   => Package['opendkim'];
    }

    file {
      '/etc/dkim':
        ensure  => directory,
        require => Package['opendkim'],
        owner   => 'opendkim',
        group   => 'opendkim',
        mode    => '0700';
      '/etc/dkim/keys':
        ensure  => directory,
        require => File['/etc/dkim'],
        owner   => 'opendkim',
        group   => 'opendkim',
        mode    => '0700'
    }

    concat { '/etc/dkim/KeyTable':
      ensure => present,
      owner  => 'opendkim',
      group  => 'root',
      mode   => '0600',
      notify => Service['opendkim']
    }

    concat { '/etc/dkim/SigningTable':
      ensure => present,
      owner  => 'opendkim',
      group  => 'root',
      mode   => '0600',
      notify => Service['opendkim']
    }

    $dkim_keys.each |Integer $index, Hash[Enum['domainname', 'dkim_domainkey', 'dkim_domainkey_private'], String] $dkim_key| {

      file {
        "/etc/dkim/keys/${dkim_key['domainname']}":
          ensure  => directory,
          require => File['/etc/dkim/keys'],
          owner   => 'opendkim',
          group   => 'opendkim',
          mode    => '0700';
        "/etc/dkim/keys/${dkim_key['domainname']}/${::hostname}.private":
          require => File["/etc/dkim/keys/${dkim_key['domainname']}"],
          content => $dkim_key['dkim_domainkey_private'],
          owner   => 'opendkim',
          group   => 'opendkim',
          mode    => '0600';
      }

      concat::fragment { "keytable ${index}":
        target  => '/etc/dkim/KeyTable',
        content => "${::hostname}._domainkey.${dkim_key['domainname']} ${dkim_key['domainname']}:${::hostname}:/etc/dkim/keys/${dkim_key['domainname']}/${::hostname}.private\n",
        order   => ($index)
      }

      concat::fragment { "signingtable ${index}":
        target  => '/etc/dkim/SigningTable',
        content => "${dkim_key['domainname']} ${::hostname}._domainkey.${dkim_key['domainname']}\n",
        order   => ($index)
      }

    }

    $trusted_hosts = "127.0.0.1
::1
localhost
"

    file { '/etc/dkim/TrustedHosts':
      content => $trusted_hosts,
      require => File['/etc/dkim'],
      notify  => Service['opendkim']
    }

    $opendkim_conf = "Syslog              yes
UMask               007
Socket              inet:8892@localhost
PidFile             /var/run/opendkim/opendkim.pid
TrustAnchorFile     /usr/share/dns/root.key
UserID              opendkim
OversignHeaders     From
KeyTable            /etc/dkim/KeyTable
SigningTable        /etc/dkim/SigningTable
ExternalIgnoreList  /etc/dkim/TrustedHosts
InternalHosts       /etc/dkim/TrustedHosts
"
    file { '/etc/opendkim.conf':
      content => $opendkim_conf,
      require => File['/etc/dkim'],
      notify  => Service['opendkim']
    }

    postfix::config {
      'milter_default_action': value => 'accept';
      'milter_protocol':       value => '2';
      'smtpd_milters':         value => 'inet:localhost:8892';
      'non_smtpd_milters':     value => 'inet:localhost:8892';
    }

  }
}
