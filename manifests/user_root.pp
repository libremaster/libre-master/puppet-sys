# Class: configuration du compte root
class sys::user_root(
  String $password
) {

  # PURGE SSH KEYS
  user { 'root':
    home           => '/root',
    password       => Sensitive($password),
    purge_ssh_keys => true,
  }

  # PROMPT ROOT
  file {
    '/root/.bashrc':
      source => "puppet:///modules/${module_name}/user_root/root/.bashrc",
      mode   => '0644',
      owner  => 'root',
      group  => 'root';
    '/root/.liquidpromptrc':
      source => "puppet:///modules/${module_name}/user_root/root/.liquidpromptrc",
      mode   => '0644',
      owner  => 'root',
      group  => 'root';
  }

}
