# Firewall
class sys::firewall (
  Boolean $ipv6,
  Integer $baserulenumber = 200,
  Integer $adminbaserulenumber = 100,
  Array[Hash] $networks = [],
  Array[Hash] $adminnetworks = [],
  Boolean $suspended = false,
) {

  firewallchain {
    'PREROUTING:nat:IPv4':
      ensure => present,
      purge  => true;
    'INPUT:filter:IPv4':
      ensure => present,
      purge  => true,
      ignore => [
        '-j f2b-(.*)',
      ];
    'FORWARD:filter:IPv4':
      ensure => present,
      purge  => true,
      ignore => [
        '-o docker0',
        '-i docker0',
        '-j DOCKER-ISOLATION',
      ];
    'OUTPUT:filter:IPv4':
      ensure => present,
      purge  => true;
    'PREROUTING:nat:IPv6':
      ensure => present,
      purge  => true;
    'INPUT:filter:IPv6':
      ensure => present,
      purge  => true,
      ignore => [
        '-j f2b-(.*)',
      ];
    'FORWARD:filter:IPv6':
      ensure => present,
      purge  => true,
      ignore => [
        '-o docker0',
        '-i docker0',
        '-j DOCKER-ISOLATION',
      ];
    'OUTPUT:filter:IPv6':
      ensure => present,
      purge  => true;
  }

  Firewall {
    before  => Class['sys::firewall_post'],
    require => Class['sys::firewall_pre'],
  }
  class { ['sys::firewall_pre', 'sys::firewall_post']: }
  class { 'firewall': }

  $adminnetworks.each |Integer $index, Hash[
      Enum['source_name', 'service_name', 'rule'],
      Variant[String, Hash[String, Variant[String, Array[Variant[String, Integer]]]]]
    ] $adminnetwork| {
    $id = ($adminbaserulenumber+$index)
    if $adminnetwork['rule']['destination'] != undef and $adminnetwork['rule']['destination'] != '' {
      if $adminnetwork['rule']['jump'] == 'DNAT' {
          $rulename = "${id} Redirect ${adminnetwork['service_name']} to (${adminnetwork['source_name']}) ${adminnetwork['rule']['todest']}"
      } else {
          $rulename = "${id} Allow ${adminnetwork['service_name']} to ${adminnetwork['source_name']}"
      }
    } else {
      $rulename = "${id} Allow ${adminnetwork['service_name']} from ${adminnetwork['source_name']}"
    }
    firewall { $rulename:
      *   => $adminnetwork['rule'],
    }
  }

  unless $suspended {

    $networks.each |Integer $index, Hash[
        Enum['source_name', 'service_name', 'rule'],
        Variant[String, Hash[String, Variant[String, Array[Variant[String, Integer]]]]]
      ] $network| {
      $id = ($baserulenumber+$index)
      if $network['rule']['destination'] != undef and $network['rule']['destination'] != '' {
        if $network['rule']['jump'] == 'DNAT' {
            $rulename = "${id} Redirect ${network['service_name']} to (${network['source_name']}) ${network['rule']['todest']}"
        } else {
            $rulename = "${id} Allow ${network['service_name']} to ${network['source_name']}"
        }
      } else {
        $rulename = "${id} Allow ${network['service_name']} from ${network['source_name']}"
      }
      firewall { $rulename:
        *   => $network['rule'],
      }
    }

  }

}
