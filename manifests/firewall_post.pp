# Firewall, last rule
class sys::firewall_post {

  firewall { '999 drop all':
    proto  => 'all',
    action => 'drop',
    before => undef,
  }

  if ($sys::firewall::ipv6) {
    firewall { '999 drop all (v6)':
      proto    => 'all',
      action   => 'drop',
      before   => undef,
      provider => 'ip6tables',
    }
  }

}
