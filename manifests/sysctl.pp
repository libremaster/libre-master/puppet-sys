# Class: SYS / sysctl
class sys::sysctl (
  Array[Hash] $confs = [],
) {

  exec { 'update_sysctl':
    command     => '/sbin/sysctl -e -p /etc/sysctl.conf',
    refreshonly => true
  }

  $confs.each |Integer $index, Hash[Enum['name', 'value'], String] $conf| {
    augeas {
      "sysctl_${conf['name']}":
        context => '/files/etc/sysctl.conf',
        changes => [
          "set ${conf['name']} \"${conf['value']}\""
        ],
        onlyif  => "get ${conf['name']} != \"${conf['value']}\"",
        notify  => Exec['update_sysctl'];
    }
  }

}
