# Class: SYS / APT
class sys::update (
  String $email_notification,
) {

    # MAJ AUTO
    package {'unattended-upgrades': ensure => present }

    # Notifications MAJ DISPO
    package {'apticron': ensure => present }
    file {
      '/etc/apticron/apticron.conf':
        content => template('sys/apt/etc/apticron/apticron.conf'),
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        require => Package['apticron'];
    }

}
