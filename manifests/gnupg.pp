# Class: SYS / APT
class sys::gnupg {

    # gnupg soit être installé en premier à cause d'une dépendance de apt
    ensure_packages(['gnupg'])

}
