# Firewall, first rules
class sys::firewall_pre (
  String $udp_limit = '',
  Integer $udp_burst = 0,
) {

  Firewall {
    require => undef,
  }

  # Default firewall rules - IPv4
  firewall { '000 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }
  -> firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }
  -> firewall { '003 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }

  if ($udp_limit != '') {
    firewallchain {
      'UDP-PROTECT:filter:IPv4':
        ensure => present,
        purge  => true;
    }
    -> firewall { '010 UDP Flood protect':
      chain => 'UDP-PROTECT',
      proto => 'udp',
      limit => $udp_limit,
      burst => $udp_burst,
      jump  => 'RETURN',
    }
    -> firewall { '011 UDP Flood log':
      chain      => 'UDP-PROTECT',
      proto      => 'udp',
      log_level  => 4,
      log_prefix => 'UDP-flood attempt: ',
      jump       => 'LOG',
    }
    -> firewall { '012 UDP Flood DROP':
      chain  => 'UDP-PROTECT',
      action => 'drop',
    }
  }

  if ($sys::firewall::ipv6) {
    # Default firewall rules - IPv6
    firewall { '000 accept all icmp (v6)':
      proto    => 'ipv6-icmp',
      action   => 'accept',
      provider => 'ip6tables',
    }
    -> firewall { '001 accept all to lo interface (v6)':
      proto    => 'all',
      iniface  => 'lo',
      action   => 'accept',
      provider => 'ip6tables',
    }
    -> firewall { '003 accept related established rules (v6)':
      proto    => 'all',
      state    => ['RELATED', 'ESTABLISHED'],
      action   => 'accept',
      provider => 'ip6tables',
    }
  }

}
