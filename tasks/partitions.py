#!/usr/bin/env python

import sys
if sys.version_info[0] == 2 and sys.version_info[1] < 5:
  import simplejson as json
else:
  import json
import subprocess

message = ""
result = {'message': message, 'partitions': {}}

# Read parameters
def make_serializable(object):
  if sys.version_info[0] > 2:
    return object
  if isinstance(object, unicode):
    return object.encode('utf-8')
  else:
    return object

data = json.load(sys.stdin)

action = None
partition = None

if 'action' in data:
  action = make_serializable(data['action'])
if 'partition' in data:
  partition = make_serializable(data['partition'])

if action == 'list':
  # List partition facts
  process = subprocess.Popen(['/opt/puppetlabs/bin/facter', '-j', 'partitions'],
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
  stdout, stderr = process.communicate()
  result['partitions'] = json.loads(stdout)
  print(json.dumps(result))

if action == 'get':
  # Get partition facts
  process = subprocess.Popen(['/opt/puppetlabs/bin/facter', '-j', 'partitions.' + partition],
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
  stdout, stderr = process.communicate()
  result['partitions'] = json.loads(stdout)
  print(json.dumps(result))

if action == 'mkext4':
  # Format partition
  process = subprocess.Popen(['/usr/sbin/mkfs.ext4', partition],
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
  stdout, stderr = process.communicate()
  result['message'] = stdout
  print(json.dumps(result))
