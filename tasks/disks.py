#!/usr/bin/env python

import sys
if sys.version_info[0] == 2 and sys.version_info[1] < 5:
  import simplejson as json
else:
  import json
import subprocess

message = ""
result = {'message': message, 'disks': {}}

# Read parameters
def make_serializable(object):
  if sys.version_info[0] > 2:
    return object
  if isinstance(object, unicode):
    return object.encode('utf-8')
  else:
    return object

data = json.load(sys.stdin)

action = None
disk = None

if 'action' in data:
  action = make_serializable(data['action'])
if 'disk' in data:
  disk = make_serializable(data['disk'])

if action == 'list':
  # List partition facts
  process = subprocess.Popen(['/opt/puppetlabs/bin/facter', '-j', 'disks'],
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
  stdout, stderr = process.communicate()
  result['disks'] = json.loads(stdout)
  print(json.dumps(result))

if action == 'get':
  # Get disk facts
  process = subprocess.Popen(['/opt/puppetlabs/bin/facter', '-j', 'disks.' + disk],
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
  stdout, stderr = process.communicate()
  result['disks'] = json.loads(stdout)
  print(json.dumps(result))

if action == 'mkpart':
  # Create partition
  process = subprocess.Popen(['/usr/sbin/sgdisk', '-N', '0', '/dev/' + disk],
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
  stdout, stderr = process.communicate()
  result['message'] = stdout
  print(json.dumps(result))
