# nom hote de la forme abc123def -> récupère abc
Facter.add('port80') do
  setcode do
    Facter::Core::Execution.execute('/bin/ss -lnt src :80 | /bin/grep LISTEN > /dev/null && echo "OK"') == 'OK'
  end
end

Facter.add('port443') do
  setcode do
    Facter::Core::Execution.execute('/bin/ss -lnt src :443 | /bin/grep LISTEN > /dev/null && echo "OK"') == 'OK'
  end
end

Facter.add('port22') do
  setcode do
    Facter::Core::Execution.execute('/bin/ss -lnt src :22 | /bin/grep LISTEN > /dev/null && echo "OK"') == 'OK'
  end
end
